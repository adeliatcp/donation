import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthComponent } from './auth.component';
import { LandingComponent } from './landing/landing.component';

const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
    {
        path: '',
        component: LandingComponent
    },
    {
        path: 'admin-login',
        component: LoginComponent
    }]
  },
  {
        path: '',
        redirectTo: 'admin-login',
        pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
