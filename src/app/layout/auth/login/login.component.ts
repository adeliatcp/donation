import { Component } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { HotToastService } from '@ngneat/hot-toast';
import { AuthService } from 'src/app/services/auth.service';
import { AnimationOptions } from 'ngx-lottie';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  noHp!: string;
  pin!: string;
  isPin: boolean = true;

  formLogin!: FormGroup;

  constructor(
    private authService: AuthService,
    private toastService: HotToastService,
    private router: Router
  ) {
    this.formLogin = new FormGroup({
       noHp: new FormControl<string | null>("", { validators: [Validators.required, Validators.minLength(10)]}),
       pin: new FormControl<string | null>("", { validators: [Validators.required, Validators.minLength(4)]}),
    });
  }

  get errorControl(){
    return this.formLogin.controls;
  }

  doChangeTypePin(){
    this.isPin = !this.isPin
  }


  lottieConfig: AnimationOptions = {
    path: 'assets/119479-avatars-multiple.json'
  }


  doLogin() {
    console.log(this.formLogin.value)
    const payload = {
      noHp: this.formLogin.value.noHp,
      pin: this.formLogin.value.pin,
    };
    this.authService.login(payload).subscribe({
      next: (val) => {
        if(val.role == "Admin") {
        console.log(val);
        this.toastService.success('Login Successfull');
        localStorage.setItem('token', val.jwtToken)
        this.authService.token = val.jwtToken
        this.router.navigate(['admin/data-donation']);
        } else {
          this.toastService.error('Login Invalid');
        }
      },
      error: (err) => {
        console.log(err);
        this.toastService.error('Login Failed');
      },
    });
  }
}
