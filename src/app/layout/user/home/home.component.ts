import { DecimalPipe } from '@angular/common';
import { AfterViewInit, Component, OnInit, PipeTransform, TemplateRef, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Chart } from 'chart.js';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AnimationOptions } from 'ngx-lottie';
import { map, Observable, startWith } from 'rxjs';

interface Country {
  name: string;
  flag: string;
  area: number;
  population: number;
}

const COUNTRIES: Country[] = [
  {
    name: 'Russia',
    flag: 'f/f3/Flag_of_Russia.svg',
    area: 17075200,
    population: 146989754
  },
  {
    name: 'Canada',
    flag: 'c/cf/Flag_of_Canada.svg',
    area: 9976140,
    population: 36624199
  },
  {
    name: 'United States',
    flag: 'a/a4/Flag_of_the_United_States.svg',
    area: 9629091,
    population: 324459463
  },
  {
    name: 'China',
    flag: 'f/fa/Flag_of_the_People%27s_Republic_of_China.svg',
    area: 9596960,
    population: 1409517397
  }
];

function search(text: string, pipe: PipeTransform): Country[] {
  return COUNTRIES.filter(country => {
    const term = text.toLowerCase();
    return country.name.toLowerCase().includes(term)
        || pipe.transform(country.area).includes(term)
        || pipe.transform(country.population).includes(term);
  });
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [DecimalPipe]
})


export class HomeComponent implements AfterViewInit {

  canvas: any;
  ctx: any;
  @ViewChild('pieCanvas') pieCanvas!: { nativeElement: any };

  pieChart: any;


  countries$: Observable<Country[]>;
  filter = new FormControl('', {nonNullable: true});


lottieConfig: AnimationOptions = {
  path: 'assets/90472-saving-the-money.json'
}

modalRef?: BsModalRef;

constructor(pipe: DecimalPipe, private modalService: BsModalService) {
  this.countries$ = this.filter.valueChanges.pipe(
    startWith(''),
    map(text => search(text, pipe))
  );
}

ngAfterViewInit(): void {
  this.pieChartBrowser();
}

pieChartBrowser(): void {
  this.canvas = this.pieCanvas.nativeElement;
  this.ctx = this.canvas.getContext('2d');

  this.pieChart = new Chart(this.ctx, {
    type: 'pie',
    data: {
      labels: ['Apple', 'Google', 'Facebook', 'Infosys', 'Hp', 'Accenture'],
      datasets: [
        {
          backgroundColor: [
            '#2ecc71',
            '#3498db',
            '#95a5a6',
            '#9b59b6',
            '#f1c40f',
            '#e74c3c',
          ],
          data: [12, 19, 3, 17, 28, 24],
        },
      ],
    },
  });
}

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }


}