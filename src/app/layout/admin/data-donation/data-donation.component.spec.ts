import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataDonationComponent } from './data-donation.component';

describe('DataDonationComponent', () => {
  let component: DataDonationComponent;
  let fixture: ComponentFixture<DataDonationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataDonationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DataDonationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
