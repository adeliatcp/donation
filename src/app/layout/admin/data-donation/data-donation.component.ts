import { DecimalPipe, provideCloudinaryLoader } from '@angular/common';
import { Component, PipeTransform, TemplateRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HotToastService } from '@ngneat/hot-toast';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { firstValueFrom, map, Observable, startWith } from 'rxjs';
import { DataDonationsService } from 'src/app/services/data-donations.service';
import { UploadFileServiceService } from 'src/app/services/upload-file-service.service';

export interface Program {
  programID: string;
  companyName: string;
  nameProgram: string;
  target: number;
  startDate: Date;
  endDate: Date;
  picURL: File;
  description: String;
  collectedFunds: number;
  netDonation: number;
  createdBy: created;
}

export interface created {
  email: string;
  id: number;
  nama: string;
  noHp: string;
}


function search(text: string, dt: Program[], pipe: PipeTransform) {
  return dt.filter((donate) => {
    const term = text.toLowerCase();
    return (
      donate.nameProgram.toLowerCase().includes(term) ||
      donate.programID.toLowerCase().includes(term) ||
      donate.companyName.toLowerCase().includes(term)
    );
  });
}

@Component({
  selector: 'app-data-donation',
  templateUrl: './data-donation.component.html',
  styleUrls: ['./data-donation.component.scss'],
  providers: [DecimalPipe],
})
export class DataDonationComponent {
  modalRef?: BsModalRef;

  page: number = 1;
  max: boolean = false;
  min: boolean = true;
  show: boolean = false;
  bsValue = new Date();

  programID!: string;
  companyName!: string;
  nameProgram!: string;
  target!: number;
  startDate!: Date;
  endDate!: Date;
  picURL!: File;
  description!: String
  collectedFunds!: number;
  netDonation!: number;
  createdBy! : created;

  program!: Observable<Program[]>;
  donation!: Program[];
  viewData!: Program;
  filter = new FormControl('', { nonNullable: true });

  constructor(
    private dataDonationsService: DataDonationsService,
    private pipe: DecimalPipe,
    private uploadFileService: UploadFileServiceService,
    private modalService: BsModalService
  ) {}

  async ngOnInit() {
    this.loadData()
  }

  async loadData() {
    try {
      const result = await firstValueFrom(
        this.dataDonationsService.getProgram()
      );
      console.log(result);
      this.program! = this.filter.valueChanges.pipe(
        startWith(''),
        map((text) => search(text, result, this.pipe))
      );
    } catch (e) {
      console.log(e);
    }
  }

  next(n: number) {
    this.page = n;
  }

  myFunction() {
    if (this.show == false) {
      this.show = true;
    } else {
      this.show = false;
    }
  }

  inputData() {
    const payload = {
      programID: this.programID,
      companyName: this.companyName,
      nameProgram: this.nameProgram,
      target: this.target,
      startDate: this.startDate,
      endDate: this.endDate,
      picURL: this.picURL,
      description: this.description,
      collectedFunds: this.collectedFunds,
      netDonation: this.netDonation,
      createdBy: this.createdBy
    };    
  
    this.dataDonationsService.newProgram(payload).subscribe({
      next: (val) => {
        console.log(val);
        this.show = false;
        alert('Berhasil!');
        this.loadData();
      },
      error: (err) => {
        console.log(err);
        alert('Gagal, Cek kembali data anda!');
        this.modalRef?.hide()
      },
    });
  }

  chooseFile(event: Event) {
    //Select File
    const target = event.target as HTMLInputElement;
    const file = target.files as FileList;
    this.picURL = file[0];
  }

  submitFile() {
    const payload = {
      file: this.picURL,
    };
    this.uploadFileService.postFile(payload).subscribe({
      next: (val: any) => {
        console.log(val);
      },
      error: (error) => {
        console.log(error);
      },
    });
  }

  tooMuch() {
    if (this.page < 6) {
      this.page++;
    } else {
      this.max = true;
      this.min = false;
    }
  }

  tooShort() {
    if (this.page > 1) {
      this.page--;
    } else {
      this.min = true;
      this.max = false;
    }
  }

  openModal(template: TemplateRef<any>, data: Program) {
    console.log(data);
    this.viewData = data;
    this.modalRef = this.modalService.show(template);
  }

  openModalInput(templateInput: TemplateRef<any>) {
    this.modalRef = this.modalService.show(templateInput);
  }
}
