import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminRoutingModule } from './admin-routing.module';
import { HistoryComponent } from './history/history.component';
import { LottieModule } from 'ngx-lottie';
import { DataDonationComponent } from './data-donation/data-donation.component';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxMaskModule } from 'ngx-mask';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { RegisterComponent } from './register/register.component';
import { AuthComponent } from '../auth/auth.component';
import { OpenProgramComponent } from './open-program/open-program.component';
import { HistoryCloseprogramComponent } from './history-closeprogram/history-closeprogram.component';

@NgModule({
  declarations: [
    AdminComponent,
    HistoryComponent,
    DataDonationComponent,
    RegisterComponent,
    OpenProgramComponent,
    HistoryCloseprogramComponent
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    AdminRoutingModule,
    LottieModule,
    TranslateModule,
    NgbModule,
    NgxMaskModule.forChild(),
    BsDatepickerModule.forRoot(),
  ]
})
export class AdminModule { }
