import { DecimalPipe } from '@angular/common';
import { Component, OnInit, PipeTransform, TemplateRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { firstValueFrom, map, Observable, startWith } from 'rxjs';
import { HistoryCloseprogramService } from 'src/app/services/history-closeprogram.service';


export interface CloseProgram {
  verifID: number;
  createdBy: created;
  programID: number;
  companyName: string;
  nameProgram: string;
  target: number;
  startDate: Date;
  endDate: Date;
  nominal: number;
  description: string;
  collectedFunds: number;
  netDonation: number;
}

export interface created {
  email: string;
  id: number;
  nama: string;
  noHp: string;
}

function search(text: string, dt: CloseProgram [], pipe: PipeTransform) {
  return dt.filter((donate) => {
    const term = text.toLowerCase();
    return (
      donate.nameProgram.toLowerCase().includes(term) ||
      donate.companyName.toLowerCase().includes(term)
    );
  });
}

@Component({
  selector: 'app-history-closeprogram',
  templateUrl: './history-closeprogram.component.html',
  styleUrls: ['./history-closeprogram.component.scss'],
  providers: [DecimalPipe]
})
export class HistoryCloseprogramComponent implements OnInit {

  modalRef?: BsModalRef;

  program!: Observable<CloseProgram[]>;
  filter = new FormControl('', {nonNullable: true});
  status!: string;

  page: number = 0;
  max: boolean = false;
  min: boolean = true;
  viewData! : CloseProgram
  change: boolean = false;

  constructor(
    private closeProgramService: HistoryCloseprogramService,
    private modalService: BsModalService,
    private pipe: DecimalPipe) { }

    async ngOnInit() {
      try {
        const result = await firstValueFrom(
           this.closeProgramService.getProgram()
        );
        console.log(result)
        this.program! = this.filter.valueChanges.pipe(
          startWith(''),
          map((text) => search(text, result, this.pipe))
        );
      } catch (e) {
        console.log(e);
      }
    }
  
    myFunction() {
      if (this.change == true) {
        this.change = false;
      } else {
        this.change = true;
      }
    }
  
  
    next(n: number) {
      this.page = n;
    }
  
    tooMuch() {
      if (this.page < 6) {
        this.page++;
      } else {
        this.max = true;
        this.min = false;
      }
    }
  
    tooShort() {
      if (this.page > 1) {
        this.page--;
      } else {
        this.min = true;
        this.max = false;
      }
    }
  
    openModal(template: TemplateRef<any>, data: CloseProgram) {
      console.log(data)
      this.viewData = data
      this.modalRef = this.modalService.show(template);
    }

}
