import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryCloseprogramComponent } from './history-closeprogram.component';

describe('HistoryCloseprogramComponent', () => {
  let component: HistoryCloseprogramComponent;
  let fixture: ComponentFixture<HistoryCloseprogramComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoryCloseprogramComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HistoryCloseprogramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
