import { Component, OnInit, PipeTransform, TemplateRef } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { OpenProgramService } from 'src/app/services/open-program.service';
import { FormControl } from '@angular/forms';
import { firstValueFrom, map, Observable, startWith } from 'rxjs';

export interface openProgram {
  programID: number;
  companyName: string;
  nameProgram: string;
  leadProgramName: string;
  leadProgramEmail: string;
  leadProgramNoHp: string;
  target: number;
  startDate: number;
  endDate: number;
  picURL: string;
  description: string;
  status: string;
  detailStatus: string;
  reviewedBy: reviewed;
}

export interface reviewed {
  email: string;
  programID: number;
  nama: string;
  noHp: string;
}

function search(text: string, dt: openProgram[], pipe: PipeTransform) {
  return dt.filter((donate) => {
    const term = text.toLowerCase();
    return (
      donate.companyName.toLowerCase().includes(term) ||
      donate.nameProgram.toLowerCase().includes(term) ||
      donate.status.toLowerCase().includes(term)
    );
  });
}

@Component({
  selector: 'app-open-program',
  templateUrl: './open-program.component.html',
  styleUrls: ['./open-program.component.scss'],
  providers: [DecimalPipe]
})
export class OpenProgramComponent implements OnInit {
  modalRef?: BsModalRef;

  open!: Observable<openProgram[]>;
  // person: Person[] = [];

  filter = new FormControl('', {nonNullable: true});

  page: number = 1;
  max: boolean = false;
  min: boolean = true;
  viewData! : openProgram
  change: boolean = false;
  programID!: number;
  status!: string;
  detailStatus!: string;

  constructor(
    private openProgramService: OpenProgramService,
    private modalService: BsModalService,
    private pipe: DecimalPipe
  ) { }

  async ngOnInit() {
    this.loadData()
  }

  async loadData() {
    try {
      const result = await firstValueFrom(
         this.openProgramService.getOpenProgram()
      );
      console.log(result)
      this.open! = this.filter.valueChanges.pipe(
        startWith(''),
        map((text) => search(text, result, this.pipe))
      );
    } catch (e) {
      console.log(e);
    }
  }

  myFunction() {
    if (this.change == true) {
      this.change = false;
    } else {
      this.change = true;
    }
  }

  updateDataUnvalid(id: number) {
    this.openProgramService.updateProgramUnvalid(id).subscribe({
      next: (val) => {
        console.log(val);
        this.loadData();
      },
      error: (err) => {
        console.log(err);
      },
    });
  }

  updateDataValid(id: number) {
    this.openProgramService.updateProgramValid(id).subscribe({
      next: (val) => {
        console.log(val);
        this.loadData();
      },
      error: (err) => {
        console.log(err);
      },
    });
  }

  next(n: number) {
    this.page = n;
  }

  tooMuch() {
    if (this.page < 6) {
      this.page++;
    } else {
      this.max = true;
      this.min = false;
    }
  }

  tooShort() {
    if (this.page > 1) {
      this.page--;
    } else {
      this.min = true;
      this.max = false;
    }
  }

  openModal(template: TemplateRef<any>, data: openProgram) {
    console.log(data)
    this.viewData = data
    this.modalRef = this.modalService.show(template);
  }

}
