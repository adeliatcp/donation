import { Component, OnInit, PipeTransform, TemplateRef } from '@angular/core';
import { TransactionService } from 'src/app/services/transaction.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { firstValueFrom, map, Observable, pipe, startWith } from 'rxjs';
import { FormControl } from '@angular/forms';
import { DecimalPipe, NgIfContext } from '@angular/common';

export interface Transactions {
  nama: string;
  programID: string;
  nameProgram: string;
  nominal: number;
  transactionDate: string;
  status: string;
}

function search(text: string, dt: Transactions[], pipe: PipeTransform) {
  return dt.filter((donate) => {
    const term = text.toLowerCase();
    return (
      donate.nama.toLowerCase().includes(term) ||
      donate.nameProgram.toLowerCase().includes(term) ||
      donate.transactionDate.toLowerCase().includes(term)
    );
  });
}

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
  providers: [DecimalPipe]
})

export class HistoryComponent implements OnInit {
  modalRef?: BsModalRef;

  trans!: Observable<Transactions[]>;
  filter = new FormControl('', {nonNullable: true});
  status!: string;

  page: number = 0;
  max: boolean = false;
  min: boolean = true;
  viewData! : Transactions
  change: boolean = false;

  constructor(
    private transactionService: TransactionService,
    private modalService: BsModalService,
    private pipe: DecimalPipe
  ) {
  }

  async ngOnInit() {
    try {
      const result = await firstValueFrom(
         this.transactionService.getTransaction()
      );
      console.log(result)
      this.trans! = this.filter.valueChanges.pipe(
        startWith(''),
        map((text) => search(text, result, this.pipe))
      );
    } catch (e) {
      console.log(e);
    }
  }

  myFunction() {
    if (this.change == true) {
      this.change = false;
    } else {
      this.change = true;
    }
  }


  next(n: number) {
    this.page = n;
  }

  tooMuch() {
    if (this.page < 6) {
      this.page++;
    } else {
      this.max = true;
      this.min = false;
    }
  }

  tooShort() {
    if (this.page > 1) {
      this.page--;
    } else {
      this.min = true;
      this.max = false;
    }
  }

  openModal(template: TemplateRef<any>, data: Transactions) {
    console.log(data)
    this.viewData = data
    this.modalRef = this.modalService.show(template);
  }

}

