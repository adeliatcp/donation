import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { HotToastService } from '@ngneat/hot-toast';
import { AnimationOptions } from 'ngx-lottie';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent {
  noHp!: string;
  nama!: string;
  pin!: string;
  email!: string;
  isPin: boolean = true;

  formRegister!: FormGroup

  constructor(
    private authService: AuthService,
    private toastService: HotToastService,
    private router: Router
  ) {
    this.formRegister = new FormGroup({
      noHp: new FormControl<string | null>("", { validators: [Validators.required, Validators.minLength(10)]}),
      nama: new FormControl<string | null>("", { validators: [Validators.required, Validators.required]}),
      pin: new FormControl<string | null>("", { validators: [Validators.required, Validators.minLength(4)]}),
      email: new FormControl<string | null>("", { validators: [Validators.required, Validators.email]})
   });
  }

  get errorControl(){
    return this.formRegister.controls;
  }

  doRegister() {
    console.log(this.formRegister.value)
    const payload = {
      noHp: this.formRegister.value.noHp,
      nama: this.formRegister.value.nama,
      pin: this.formRegister.value.pin,
      email: this.formRegister.value.email,
    };
    this.authService.register(payload).subscribe({
      next: (val) => {
        console.log(val);
        this.toastService.success('Register Succesfull');
        this.router.navigate(['/admin/information']);
      },
      error: (err) => {
        console.log(err);
        this.toastService.error('Register Failed');
      },
    });
  }

  doChangeTypePin() {
    this.isPin = !this.isPin;
  }

  lottieConfig: AnimationOptions = {
    path: 'assets/119479-avatars-multiple.json',
  };
}
