import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { HistoryComponent } from './history/history.component';
import { DataDonationComponent } from './data-donation/data-donation.component';
import { RegisterComponent } from './register/register.component';
import { OpenProgramComponent } from './open-program/open-program.component';
import { HistoryCloseprogramComponent } from './history-closeprogram/history-closeprogram.component';


const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
    //   {
    //     path: 'playground',
    //     component: PlaygroundComponent
    // },
    {
      path: 'open-program',
      component: OpenProgramComponent
    },
    {
        path: 'program-closed',
        component: HistoryCloseprogramComponent
    },
    {
      path: 'history',
      component: HistoryComponent
  },
  {
    path: 'data-donation',
    component: DataDonationComponent
},
// {
//   path: 'information',
//   component: InformationComponent
// },
{
  path: 'register',
  component: RegisterComponent
}
]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
