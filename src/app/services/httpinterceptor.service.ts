import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HotToastService } from '@ngneat/hot-toast';
import { catchError, Observable, throwError } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class HttpinterceptorService implements HttpInterceptor {

  constructor(private authService: AuthService,
    private router: Router,
    private toastService: HotToastService
    ) { 
    this.authService.token
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // console.log('HttpinterceptorService', req)
    // console.log(this.authService.token)
    if (this.authService.token)
    {
      req = req.clone({
        headers: req.headers.set('authorization', 'Bearer ' + this.authService.token)
      })
    }
    return next.handle(req).pipe(
      catchError((response: HttpErrorResponse) => {
        //console.log(response)
        if(response.status == 401){
          this.authService.token = ''
          // localStorage.removeItem('token')
          // this.toastService.error('Anda tidak memiliki akses')
          // this.router.navigate(['/admin-login'])
        }
        return throwError(response)
      })
    )
  }
}
