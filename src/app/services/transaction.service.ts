import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Transactions } from '../layout/admin/history/history.component';


@Injectable({
  providedIn: 'root'
})
export class TransactionService {


  private readonly baseUrl = 'http://localhost:8086'
  
  constructor(private httpClient: HttpClient) { }

  getTransaction(): Observable<Transactions[]>{
    return this.httpClient.get<Transactions[]>(`${this.baseUrl}/historyTransaction`)
  }
}
