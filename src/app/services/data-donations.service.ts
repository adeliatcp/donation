import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Program } from '../layout/admin/data-donation/data-donation.component';

@Injectable({
  providedIn: 'root'
})
export class DataDonationsService {

  private readonly baseUrl = 'http://localhost:8086'
  
  constructor(private httpClient: HttpClient) { }

  getProgram(): Observable<Program[]>{
    // const token = localStorage.getItem('token')
    // const headers = new HttpHeaders({
    // authorization:'Bearer ' + token
    // })
    return this.httpClient.get<Program[]>(`${this.baseUrl}/data-donation`)
  }

  newProgram(payload: Program): Observable<Program> {
    return this.httpClient.post<Program>(
      `${this.baseUrl}/data-donation`,
      payload
    );
  }

  updateUser(payload: Program): Observable<Program> {
    return this.httpClient.patch<Program>(`${this.baseUrl}/${payload.programID}`, payload);
  }

}
