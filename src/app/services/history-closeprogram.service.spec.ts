import { TestBed } from '@angular/core/testing';

import { HistoryCloseprogramService } from './history-closeprogram.service';

describe('HistoryCloseprogramService', () => {
  let service: HistoryCloseprogramService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HistoryCloseprogramService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
