import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CloseProgram } from '../layout/admin/history-closeprogram/history-closeprogram.component';

@Injectable({
  providedIn: 'root'
})
export class HistoryCloseprogramService {

  private readonly baseUrl = 'http://localhost:8086'
  
  constructor(private httpClient: HttpClient) { }

  getProgram(): Observable<CloseProgram[]>{
    return this.httpClient.get<CloseProgram[]>(`${this.baseUrl}/historyProgram`)
  }
}
