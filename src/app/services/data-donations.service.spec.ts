import { TestBed } from '@angular/core/testing';

import { DataDonationsService } from './data-donations.service';

describe('DataDonationsService', () => {
  let service: DataDonationsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataDonationsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
