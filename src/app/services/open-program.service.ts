import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { openProgram } from '../layout/admin/open-program/open-program.component';


export interface open{
  status: string;
  detailStatus: string;
}

@Injectable({
  providedIn: 'root'
})
export class OpenProgramService {

  

  private readonly baseUrl = 'http://localhost:8086'

  constructor(private httpClient: HttpClient) { }

  getOpenProgram(): Observable<openProgram[]>{
    return this.httpClient.get<openProgram[]>(`${this.baseUrl}/open-donation`)
  }

  updateProgramValid(programID: number): Observable<open[]> {
    return this.httpClient.patch<open[]>(`${this.baseUrl}/open-donation/valid-program/${programID}`, null);
  }

  updateProgramUnvalid(programID: number): Observable<open[]> {
    return this.httpClient.patch<open[]>(`${this.baseUrl}/open-donation/unvalid-program/${programID}`, null);
  }

  getProgram(programID: number): Observable<Object> {  
    return this.httpClient.get(`${this.baseUrl}/open-donation/${programID}`);  
  }  
}
