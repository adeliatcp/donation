import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { responseLogin } from './auth.service';


@Injectable({
  providedIn: 'root'
})
export class UploadFileServiceService {

  constructor(private http: HttpClient) { }

  url = 'http://localhost:8085';
  //url = environment.api

  postFile(payload: any): Observable<responseLogin>{
    let formParams = new FormData();
    formParams.append('imageFile', payload.selectedFile);
    return this.http.post<responseLogin>(`${this.url}/data-donation`, formParams)
  }
}
