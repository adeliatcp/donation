import { HttpClient, HttpResponseBase } from '@angular/common/http';
import { ReturnStatement } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, tap } from 'rxjs';

interface requestLogin {
  noHp: string;
  pin: string;
}

export interface responseLogin {
  message: string;
  jwtToken: string;
  role: string;
}

interface requestRegister {
  noHp: string;
  nama: string;
  pin: string;
  email: string;
}

interface responseRegister {
  message: string;
}

interface responseBase<T> {
  message: string;
  data: T;
}

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private readonly baseUrl = 'http://localhost:8086'

  private _token: string = '';

  public isAuthenticated: BehaviorSubject<boolean> =
    new BehaviorSubject<boolean>(false);

  constructor(private httpClient: HttpClient) {
    this.loadToken();
  }

  /**
   *
   * @param payload
   * @returns
   */

  login(payload: requestLogin): Observable<responseLogin> {
    return this.httpClient
      .post<responseLogin>(`${this.baseUrl}/login`, payload)
      .pipe(
        tap(() => {
          this.isAuthenticated.next(true);
        })
      );
  }

  /**
   *
   * @param payload
   * @returns
   */
  register(payload: requestRegister): Observable<responseRegister> {
    return this.httpClient.post<responseRegister>(
      `${this.baseUrl}/admin-register`,
      payload
    );
  }

  loadToken() {
    const token = localStorage.getItem('token');
    if (token) {
      this.token = token;
      this.isAuthenticated.next(true);
    } else {
      this.isAuthenticated.next(false);
    }
  }

  clearToken() {
    localStorage.clear();
    this.isAuthenticated.next(false);
  }

  set token(ev: string){
    this._token = ev
  }

  get token(){
    return this._token
  }
}
