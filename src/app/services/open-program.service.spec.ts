import { TestBed } from '@angular/core/testing';

import { OpenProgramService } from './open-program.service';

describe('OpenProgramService', () => {
  let service: OpenProgramService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OpenProgramService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
