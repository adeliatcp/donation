import { ChangeDetectionStrategy, Component} from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidenavComponent {

  dataPath = [
    {
    path: '/admin/open-program',
    name: 'SOCIAL ACTION SUBMISSION'
  },
  {
    path: '/admin/data-donation',
    name: 'DONATION PROGRAMS'
  },
  {
    path: '/admin/history',
    name: 'TRANSACTION',
  },
  // {
  //   path: '/admin/program-closed',
  //   name: 'PROGRAM CLOSED',
  // },
]

  constructor(private authService: AuthService, private translate: TranslateService) {
    
   }

  doLogout(){
   this.authService.clearToken(); 
  }

  doTranslate(language: string){
    this.translate.setDefaultLang(language);
    this.translate.use(language)
  }


}
